package com.company;

import com.opencsv.CSVWriter;
import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.parser.OpenAPIV3Parser;
import io.swagger.v3.parser.core.models.ParseOptions;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.apache.commons.collections4.ListUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        String location;
        if(args!=null &&args.length>=1)
         location=args[0];
        else{
            System.out.println("Path not submitted!");
            return;}
        ParseOptions parseOptions = new ParseOptions();
        parseOptions.setFlatten(true);

        OpenAPI openAPI = new OpenAPIV3Parser().read(location);

        /*SwaggerParseResult result = new OpenAPIParser().readContents(location, null, parseOptions);

        if (result.getMessages() != null)
            result.getMessages().forEach(System.err::println); // validation errors and warnings

        OpenAPI openAPI = result.getOpenAPI();*/
        Map<String, Schema> schemas=  openAPI.getComponents().getSchemas();
        try {
            CSVWriter csvWriter=new CSVWriter(new FileWriter("test.csv"));

            for (Map.Entry<String, Schema> entry : schemas.entrySet()) {
                System.out.println(entry.getKey() + "/" + entry.getValue());
                String schemaName= entry.getKey();
                Schema schema=entry.getValue();
                csvWriter.writeNext(new String[]{schemaName,getSchemaAttrbDesc(schema),"", schema.getDescription()!=null?schema.getDescription():""});
                writeProperties(schema,csvWriter,schemaName);
                csvWriter.writeNext(new String[]{"","",""});


            }

          //  csvWriter.writeNext(new String[]{"lvada", "lasun","bhendi","kharcha paani"});
            csvWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(openAPI);





    }

    public static void writeProperties(Schema schema,CSVWriter csvWriter,String schemaParentPath)
    {
        Map<String,Schema> schemaMap=schema.getProperties();
        if(schemaMap!=null)
        {
            for (Map.Entry<String, Schema> entry : schemaMap.entrySet()) {
                System.out.println(entry.getKey() + "/" + entry.getValue());
                String schemaName= entry.getKey();
                Schema schemaVal=entry.getValue();
                csvWriter.writeNext(new String[]{schemaParentPath+"."+schemaName,getSchemaAttrbDesc(schemaVal),isMandatory(schema,schemaName)?"Yes":"No", schemaVal.getDescription()!=null?schemaVal.getDescription():""});
                if(schemaVal.getProperties()!=null)
                {
                    writeProperties(schemaVal,csvWriter,schemaParentPath+"."+schemaName);
                }
            }
        }

    }

    public static String getSchemaAttrbDesc(Schema schema)
    {
        StringBuilder finalVal=new StringBuilder();
        finalVal.append("Type=").append(schema.getType())
                .append(schema.getDefault()!=null?"default=":"").append(schema.getDefault()!=null?schema.getDefault()+"\n":"")
                .append(schema.getName()!=null?"name=":"").append(schema.getName()!=null?schema.getName()+"\n":"")
                .append(schema.getTitle()!=null?"title=":"").append(schema.getTitle()!=null?schema.getTitle()+"\n":"")
                .append(schema.getMultipleOf()!=null?"MultipleOf=":"").append(schema.getMultipleOf()!=null?schema.getMultipleOf()+"\n":"")
                .append(schema.getMaximum()!=null?"Maximum=":"").append(schema.getMaximum()!=null?schema.getMaximum()+"\n":"")
                .append(schema.getExclusiveMaximum()!=null?"ExclusiveMaximum=":"").append(schema.getExclusiveMaximum()!=null?schema.getExclusiveMaximum()+"\n":"")
                .append(schema.getMinimum()!=null?"Minimum=":"").append(schema.getMinimum()!=null?schema.getMinimum()+"\n":"")
                .append(schema.getExclusiveMinimum()!=null?"ExclusiveMinimum=":"").append(schema.getExclusiveMinimum()!=null?schema.getExclusiveMinimum()+"\n":"")
                .append(schema.getMaxLength()!=null?"MaxLength=":"").append(schema.getMaxLength()!=null?schema.getMaxLength()+"\n":"")
                .append(schema.getMinLength()!=null?"MinLength=":"").append(schema.getMinLength()!=null?schema.getMinLength()+"\n":"")
                .append(schema.getPattern()!=null?"pattern=":"").append(schema.getPattern()!=null?schema.getPattern()+"\n":"")
                .append(schema.getMaxItems()!=null?"MaxItems=":"").append(schema.getMaxItems()!=null?schema.getMaxItems()+"\n":"")
                .append(schema.getMinItems()!=null?"MinItems=":"").append(schema.getMinItems()!=null?schema.getMinItems()+"\n":"")
                .append(schema.getUniqueItems()!=null?"UniqueItems=":"").append(schema.getUniqueItems()!=null?schema.getUniqueItems()+"\n":"")
                .append(schema.getMaxProperties()!=null?"maxProperties=":"").append(schema.getMaxProperties()!=null?schema.getMaxProperties()+"\n":"")
                .append(schema.getMinProperties()!=null?"minProperties=":"").append(schema.getMinProperties()!=null?schema.getMinProperties()+"\n":"");






        return finalVal.toString();
    }

    public static boolean isMandatory(Schema parentSchema, String childSchemaName)
    {
        List<String> requiredAttrbs= parentSchema.getRequired();
        if(requiredAttrbs==null)
            return false;
        for(String reqAttrb:requiredAttrbs)
        {
            if(childSchemaName.equals(reqAttrb))
            {
                return true;
            }
        }
        return false;


    }
}
